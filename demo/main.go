package main

import (
	lgr "gitlab.com/linear-packages/go/logger"
)

func main() {
	logger := lgr.New("main")
	logger.Debug("Debug")
	logger.Info("Info")
	logger.Warn("Warni")
	logger.Error("Error")
}
